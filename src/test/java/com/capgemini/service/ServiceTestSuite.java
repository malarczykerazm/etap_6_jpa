package com.capgemini.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AgencyServiceTest.class, CarServiceTest.class, EmployeeServiceTest.class,
		CarServiceDatesAndOptimisticLockingTest.class })
public class ServiceTestSuite {

}
