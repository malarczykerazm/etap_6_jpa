package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.CarEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CarServiceDatesAndOptimisticLockingTest {

	@Autowired
	private CarService carService;

	@Autowired
	private CarStatusService carStatusService;

	@PersistenceContext
	private EntityManager entityManager;

	@Rule
	public ExpectedException e = ExpectedException.none();

	@Test
	public void shouldThrowOptimisticLockException() throws ObjectOptimisticLockingFailureException {
		// given
		final int carID = 1;

		CarEntity car = carService.findCarByID(carID);
		CarEntity anotherCar = carService.findCarByID(carID);

		car.setColour("blue");
		entityManager.detach(car);

		anotherCar.setProductionYear(2000);
		entityManager.detach(anotherCar);

		carService.updateCar(car);
		entityManager.flush();

		// expect
		e.expect(ObjectOptimisticLockingFailureException.class);

		// when
		carService.updateCar(anotherCar);

		// then
		// EXCEPTION
	}

	@Test
	public void shouldAddCreationDateToObject() throws ParseException {
		// given
		CarEntity car = new CarEntity();
		car.setStatus(carStatusService.findCarStatusByID(1));
		car.setCarBodyType("testBodyType");
		car.setBrand("testBrand");
		car.setModel("testModel");
		car.setProductionYear(1991);
		car.setColour("testColour");
		car.setEngineVolume(new BigDecimal("1.0"));
		car.setPower(100);
		car.setMileage(100000);
		car.setPlateNumber("1234567");

		LocalDateTime currentDateTime = LocalDateTime.now();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = formatter.parse(currentDateTime.getYear() + "-" + currentDateTime.getMonthValue() + "-"
				+ currentDateTime.getDayOfMonth());

		// when
		CarEntity addedCar = carService.saveCar(car);

		// then
		assertNotNull(addedCar.getCreationDate());
		assertEquals(currentDate, addedCar.getCreationDate());
	}

	@Test
	public void shouldChangeModificationDateInObject() throws ParseException {
		// given
		final int carID = 1;
		CarEntity car = carService.findCarByID(carID);
		entityManager.detach(car);

		LocalDateTime currentDateTime = LocalDateTime.now();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = formatter.parse(currentDateTime.getYear() + "-" + currentDateTime.getMonthValue() + "-"
				+ currentDateTime.getDayOfMonth());

		// when
		car.setProductionYear(2000);
		carService.updateCar(car);
		entityManager.flush();
		CarEntity updatedCar = carService.findCarByID(car.getID());

		// then
		assertNotNull(updatedCar.getModificationDate());
		assertEquals(currentDate, updatedCar.getModificationDate());
	}

}
