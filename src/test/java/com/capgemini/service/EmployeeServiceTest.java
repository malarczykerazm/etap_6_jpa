package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.EmployeeEntity;
import com.capgemini.searchcriteria.EmployeeSearchCriteria;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class EmployeeServiceTest {

	@Autowired
	private EmployeeService employeeService;

	@Test
	public void shouldFindEmployeesByAgencySupervisedCarAndPosition() {
		// given
		final int agencyID = 1;
		final int carID = 19;
		final int positionID = 3;

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(4));

		EmployeeSearchCriteria eSC = new EmployeeSearchCriteria();
		eSC.setAgencyID(agencyID);
		eSC.setSupervisedCarID(carID);
		eSC.setPositionID(positionID);

		// when
		List<EmployeeEntity> actualEmployees = employeeService.findEmployeesByAgencySupervisedCarAndPosition(eSC);

		// then
		assertTrue(expectedEmployees.size() == actualEmployees.size());

		assertEquals(expectedEmployees.get(0).getID(), actualEmployees.get(0).getID());
		assertEquals(expectedEmployees.get(0).getFirstname(), actualEmployees.get(0).getFirstname());
		assertEquals(expectedEmployees.get(0).getSurname(), actualEmployees.get(0).getSurname());
		assertEquals(expectedEmployees.get(0).getPosition(), actualEmployees.get(0).getPosition());
		assertEquals(expectedEmployees.get(0).getAgency(), actualEmployees.get(0).getAgency());
	}

	@Test
	public void shouldFindEmployeesByAgencyAndSupervisedCar() {
		// given
		final int agencyID = 1;
		final int carID = 19;

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(4));
		expectedEmployees.add(employeeService.findEmployeeByID(11));

		EmployeeSearchCriteria eSC = new EmployeeSearchCriteria();
		eSC.setAgencyID(agencyID);
		eSC.setSupervisedCarID(carID);

		// when
		List<EmployeeEntity> actualEmployees = employeeService.findEmployeesByAgencySupervisedCarAndPosition(eSC);

		// then
		assertTrue(expectedEmployees.size() == actualEmployees.size());

		assertEquals(expectedEmployees.get(0).getID(), actualEmployees.get(0).getID());
		assertEquals(expectedEmployees.get(0).getFirstname(), actualEmployees.get(0).getFirstname());
		assertEquals(expectedEmployees.get(0).getSurname(), actualEmployees.get(0).getSurname());
		assertEquals(expectedEmployees.get(0).getPosition(), actualEmployees.get(0).getPosition());
		assertEquals(expectedEmployees.get(0).getAgency(), actualEmployees.get(0).getAgency());

		assertEquals(expectedEmployees.get(1).getID(), actualEmployees.get(1).getID());
		assertEquals(expectedEmployees.get(1).getFirstname(), actualEmployees.get(1).getFirstname());
		assertEquals(expectedEmployees.get(1).getSurname(), actualEmployees.get(1).getSurname());
		assertEquals(expectedEmployees.get(1).getPosition(), actualEmployees.get(1).getPosition());
		assertEquals(expectedEmployees.get(1).getAgency(), actualEmployees.get(1).getAgency());
	}

	@Test
	public void shouldFindEmployeesByAgencyAndPosition() {
		// given
		final int agencyID = 1;
		final int positionID = 3;

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(4));

		EmployeeSearchCriteria eSC = new EmployeeSearchCriteria();
		eSC.setAgencyID(agencyID);
		eSC.setPositionID(positionID);

		// when
		List<EmployeeEntity> actualEmployees = employeeService.findEmployeesByAgencySupervisedCarAndPosition(eSC);

		// then
		assertTrue(expectedEmployees.size() == actualEmployees.size());

		assertEquals(expectedEmployees.get(0).getID(), actualEmployees.get(0).getID());
		assertEquals(expectedEmployees.get(0).getFirstname(), actualEmployees.get(0).getFirstname());
		assertEquals(expectedEmployees.get(0).getSurname(), actualEmployees.get(0).getSurname());
		assertEquals(expectedEmployees.get(0).getPosition(), actualEmployees.get(0).getPosition());
		assertEquals(expectedEmployees.get(0).getAgency(), actualEmployees.get(0).getAgency());
	}

	@Test
	public void shouldFindEmployeesBySupervisedCarAndPosition() {
		// given
		final int carID = 19;
		final int positionID = 3;

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(4));

		EmployeeSearchCriteria eSC = new EmployeeSearchCriteria();
		eSC.setSupervisedCarID(carID);
		eSC.setPositionID(positionID);

		// when
		List<EmployeeEntity> actualEmployees = employeeService.findEmployeesByAgencySupervisedCarAndPosition(eSC);

		// then
		assertTrue(expectedEmployees.size() == actualEmployees.size());

		assertEquals(expectedEmployees.get(0).getID(), actualEmployees.get(0).getID());
		assertEquals(expectedEmployees.get(0).getFirstname(), actualEmployees.get(0).getFirstname());
		assertEquals(expectedEmployees.get(0).getSurname(), actualEmployees.get(0).getSurname());
		assertEquals(expectedEmployees.get(0).getPosition(), actualEmployees.get(0).getPosition());
		assertEquals(expectedEmployees.get(0).getAgency(), actualEmployees.get(0).getAgency());
	}

	@Test
	public void shouldFindEmployeesByAgencyOnly() {
		// given
		final int agencyID = 1;

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(4));
		expectedEmployees.add(employeeService.findEmployeeByID(5));
		expectedEmployees.add(employeeService.findEmployeeByID(11));
		expectedEmployees.add(employeeService.findEmployeeByID(20));
		expectedEmployees.add(employeeService.findEmployeeByID(23));
		expectedEmployees.add(employeeService.findEmployeeByID(24));

		EmployeeSearchCriteria eSC = new EmployeeSearchCriteria();
		eSC.setAgencyID(agencyID);

		// when
		List<EmployeeEntity> actualEmployees = employeeService.findEmployeesByAgencySupervisedCarAndPosition(eSC);

		// then
		assertTrue(expectedEmployees.size() == actualEmployees.size());

		assertEquals(expectedEmployees.get(0).getID(), actualEmployees.get(0).getID());
		assertEquals(expectedEmployees.get(0).getFirstname(), actualEmployees.get(0).getFirstname());
		assertEquals(expectedEmployees.get(0).getSurname(), actualEmployees.get(0).getSurname());
		assertEquals(expectedEmployees.get(0).getPosition(), actualEmployees.get(0).getPosition());
		assertEquals(expectedEmployees.get(0).getAgency(), actualEmployees.get(0).getAgency());

		assertEquals(expectedEmployees.get(1).getID(), actualEmployees.get(1).getID());
		assertEquals(expectedEmployees.get(1).getFirstname(), actualEmployees.get(1).getFirstname());
		assertEquals(expectedEmployees.get(1).getSurname(), actualEmployees.get(1).getSurname());
		assertEquals(expectedEmployees.get(1).getPosition(), actualEmployees.get(1).getPosition());
		assertEquals(expectedEmployees.get(1).getAgency(), actualEmployees.get(1).getAgency());

		assertEquals(expectedEmployees.get(2).getID(), actualEmployees.get(2).getID());
		assertEquals(expectedEmployees.get(2).getFirstname(), actualEmployees.get(2).getFirstname());
		assertEquals(expectedEmployees.get(2).getSurname(), actualEmployees.get(2).getSurname());
		assertEquals(expectedEmployees.get(2).getPosition(), actualEmployees.get(2).getPosition());
		assertEquals(expectedEmployees.get(2).getAgency(), actualEmployees.get(2).getAgency());

		assertEquals(expectedEmployees.get(3).getID(), actualEmployees.get(3).getID());
		assertEquals(expectedEmployees.get(3).getFirstname(), actualEmployees.get(3).getFirstname());
		assertEquals(expectedEmployees.get(3).getSurname(), actualEmployees.get(3).getSurname());
		assertEquals(expectedEmployees.get(3).getPosition(), actualEmployees.get(3).getPosition());
		assertEquals(expectedEmployees.get(3).getAgency(), actualEmployees.get(3).getAgency());

		assertEquals(expectedEmployees.get(4).getID(), actualEmployees.get(4).getID());
		assertEquals(expectedEmployees.get(4).getFirstname(), actualEmployees.get(4).getFirstname());
		assertEquals(expectedEmployees.get(4).getSurname(), actualEmployees.get(4).getSurname());
		assertEquals(expectedEmployees.get(4).getPosition(), actualEmployees.get(4).getPosition());
		assertEquals(expectedEmployees.get(4).getAgency(), actualEmployees.get(4).getAgency());

		assertEquals(expectedEmployees.get(5).getID(), actualEmployees.get(5).getID());
		assertEquals(expectedEmployees.get(5).getFirstname(), actualEmployees.get(5).getFirstname());
		assertEquals(expectedEmployees.get(5).getSurname(), actualEmployees.get(5).getSurname());
		assertEquals(expectedEmployees.get(5).getPosition(), actualEmployees.get(5).getPosition());
		assertEquals(expectedEmployees.get(5).getAgency(), actualEmployees.get(5).getAgency());
	}

	@Test
	public void shouldFindEmployeesBySupervisedCarOnly() {
		// given
		final int carID = 19;

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(4));
		expectedEmployees.add(employeeService.findEmployeeByID(11));
		expectedEmployees.add(employeeService.findEmployeeByID(9));

		EmployeeSearchCriteria eSC = new EmployeeSearchCriteria();
		eSC.setSupervisedCarID(carID);

		// when
		List<EmployeeEntity> actualEmployees = employeeService.findEmployeesByAgencySupervisedCarAndPosition(eSC);

		// then
		assertTrue(expectedEmployees.size() == actualEmployees.size());

		assertEquals(expectedEmployees.get(0).getID(), actualEmployees.get(0).getID());
		assertEquals(expectedEmployees.get(0).getFirstname(), actualEmployees.get(0).getFirstname());
		assertEquals(expectedEmployees.get(0).getSurname(), actualEmployees.get(0).getSurname());
		assertEquals(expectedEmployees.get(0).getPosition(), actualEmployees.get(0).getPosition());
		assertEquals(expectedEmployees.get(0).getAgency(), actualEmployees.get(0).getAgency());

		assertEquals(expectedEmployees.get(1).getID(), actualEmployees.get(1).getID());
		assertEquals(expectedEmployees.get(1).getFirstname(), actualEmployees.get(1).getFirstname());
		assertEquals(expectedEmployees.get(1).getSurname(), actualEmployees.get(1).getSurname());
		assertEquals(expectedEmployees.get(1).getPosition(), actualEmployees.get(1).getPosition());
		assertEquals(expectedEmployees.get(1).getAgency(), actualEmployees.get(1).getAgency());

		assertEquals(expectedEmployees.get(2).getID(), actualEmployees.get(2).getID());
		assertEquals(expectedEmployees.get(2).getFirstname(), actualEmployees.get(2).getFirstname());
		assertEquals(expectedEmployees.get(2).getSurname(), actualEmployees.get(2).getSurname());
		assertEquals(expectedEmployees.get(2).getPosition(), actualEmployees.get(2).getPosition());
		assertEquals(expectedEmployees.get(2).getAgency(), actualEmployees.get(2).getAgency());
	}

	@Test
	public void shouldFindEmployeesByPositionOnly() {
		// given
		final int positionID = 3;

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(1));
		expectedEmployees.add(employeeService.findEmployeeByID(2));
		expectedEmployees.add(employeeService.findEmployeeByID(4));
		expectedEmployees.add(employeeService.findEmployeeByID(19));
		expectedEmployees.add(employeeService.findEmployeeByID(22));

		EmployeeSearchCriteria eSC = new EmployeeSearchCriteria();
		eSC.setPositionID(positionID);

		// when
		List<EmployeeEntity> actualEmployees = employeeService.findEmployeesByAgencySupervisedCarAndPosition(eSC);

		// then
		assertTrue(expectedEmployees.size() == actualEmployees.size());

		assertEquals(expectedEmployees.get(0).getID(), actualEmployees.get(0).getID());
		assertEquals(expectedEmployees.get(0).getFirstname(), actualEmployees.get(0).getFirstname());
		assertEquals(expectedEmployees.get(0).getSurname(), actualEmployees.get(0).getSurname());
		assertEquals(expectedEmployees.get(0).getPosition(), actualEmployees.get(0).getPosition());
		assertEquals(expectedEmployees.get(0).getAgency(), actualEmployees.get(0).getAgency());

		assertEquals(expectedEmployees.get(1).getID(), actualEmployees.get(1).getID());
		assertEquals(expectedEmployees.get(1).getFirstname(), actualEmployees.get(1).getFirstname());
		assertEquals(expectedEmployees.get(1).getSurname(), actualEmployees.get(1).getSurname());
		assertEquals(expectedEmployees.get(1).getPosition(), actualEmployees.get(1).getPosition());
		assertEquals(expectedEmployees.get(1).getAgency(), actualEmployees.get(1).getAgency());

		assertEquals(expectedEmployees.get(2).getID(), actualEmployees.get(2).getID());
		assertEquals(expectedEmployees.get(2).getFirstname(), actualEmployees.get(2).getFirstname());
		assertEquals(expectedEmployees.get(2).getSurname(), actualEmployees.get(2).getSurname());
		assertEquals(expectedEmployees.get(2).getPosition(), actualEmployees.get(2).getPosition());
		assertEquals(expectedEmployees.get(2).getAgency(), actualEmployees.get(2).getAgency());

		assertEquals(expectedEmployees.get(3).getID(), actualEmployees.get(3).getID());
		assertEquals(expectedEmployees.get(3).getFirstname(), actualEmployees.get(3).getFirstname());
		assertEquals(expectedEmployees.get(3).getSurname(), actualEmployees.get(3).getSurname());
		assertEquals(expectedEmployees.get(3).getPosition(), actualEmployees.get(3).getPosition());
		assertEquals(expectedEmployees.get(3).getAgency(), actualEmployees.get(3).getAgency());

		assertEquals(expectedEmployees.get(4).getID(), actualEmployees.get(4).getID());
		assertEquals(expectedEmployees.get(4).getFirstname(), actualEmployees.get(4).getFirstname());
		assertEquals(expectedEmployees.get(4).getSurname(), actualEmployees.get(4).getSurname());
		assertEquals(expectedEmployees.get(4).getPosition(), actualEmployees.get(4).getPosition());
		assertEquals(expectedEmployees.get(4).getAgency(), actualEmployees.get(4).getAgency());
	}

	@Test
	public void shouldFindEmployeesByEmptySearchCriteria() {
		// given

		List<EmployeeEntity> expectedEmployees = employeeService.findAllEmployees();

		EmployeeSearchCriteria eSC = new EmployeeSearchCriteria();

		// when
		List<EmployeeEntity> actualEmployees = employeeService.findEmployeesByAgencySupervisedCarAndPosition(eSC);

		// then
		assertTrue(expectedEmployees.size() == actualEmployees.size());
	}

}
