package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.AddressEntity;
import com.capgemini.domain.AgencyEntity;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class AgencyServiceTest {

	@Autowired
	private AgencyService agencyService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private ContactDataService contactDataService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private CarService carService;

	@Autowired
	private CustomerService customerService;

	@Test
	public void shouldFindAllAgencies() {
		// given

		// when
		List<AgencyEntity> allAgencies = agencyService.findAllAgencies();

		// then
		assertNotNull(allAgencies);
		assertTrue(5 == allAgencies.size());
	}

	@Test
	public void shouldFindAgencyById() {
		// given
		final int agencyID = 1;

		// when
		AgencyEntity agency = agencyService.findAgencyByID(agencyID);

		// then
		assertNotNull(agency);
		assertEquals(addressService.findAddressByID(agencyID), agency.getAddress());
		assertEquals(contactDataService.findContactDataByID(agencyID), agency.getContactData());
	}

	@Test
	public void shouldSaveAgency() {
		// given
		AgencyEntity agency = new AgencyEntity();
		agency.setAddress(addressService.findAddressByID(1));
		agency.setContactData(contactDataService.findContactDataByID(1));

		int sizeBefore = agencyService.findAllAgencies().size();

		// when
		agencyService.saveAgency(agency);
		List<AgencyEntity> agencies = agencyService.findAllAgencies();

		// then
		assertEquals(agency, agencies.get(agencies.size() - 1));
		assertTrue(sizeBefore + 1 == agencies.size());
	}

	@Test
	public void shouldDeleteAgency() {
		// given
		final int agencyID = 1;
		AgencyEntity agency = agencyService.findAgencyByID(agencyID);

		int sizeBefore = agencyService.findAllAgencies().size();
		int numberOfDeletedAgencyEmployees = agency.getEmployees().size();
		int totaOfEmployeesBefore = employeeService.findAllEmployees().size();
		int totalNumberOfCustomersBefore = customerService.findAllCustomers().size();

		// when
		agencyService.deleteAgency(agency);
		List<AgencyEntity> agencies = agencyService.findAllAgencies();
		int totalNumberOfEmployeesAfter = employeeService.findAllEmployees().size();
		int totalNumberOfCustomersAfter = customerService.findAllCustomers().size();

		// then
		assertNull(agencyService.findAgencyByID(agencyID));
		assertTrue(sizeBefore - 1 == agencies.size());
		assertTrue(totaOfEmployeesBefore - numberOfDeletedAgencyEmployees == totalNumberOfEmployeesAfter);
		assertTrue(totalNumberOfCustomersBefore == totalNumberOfCustomersAfter);
	}

	@Test
	public void shouldUpdateAgency() {
		// given
		final int agencyID = 1;

		AgencyEntity agency = agencyService.findAgencyByID(agencyID);

		final AddressEntity newAddress = addressService.findAddressByID(2);
		final AddressEntity oldAddress = agency.getAddress();
		agency.setAddress(newAddress);

		int sizeBefore = agencyService.findAllAgencies().size();

		// when
		agencyService.updateAgency(agency);
		List<AgencyEntity> agencies = agencyService.findAllAgencies();

		// then
		assertTrue(newAddress == agencyService.findAgencyByID(agencyID).getAddress());
		assertTrue(newAddress != oldAddress);
		assertTrue(sizeBefore == agencies.size());
	}

	@Test
	public void shouldAddEmployeeToAgency() {
		// given
		final int agencyID = 1;
		final int employeeID = 15;

		AgencyEntity agency = agencyService.findAgencyByID(agencyID);
		EmployeeEntity employee = employeeService.findEmployeeByID(employeeID);

		final int numberOfAgencyEmployeesBefore = agencyService.findEmployeesOfAgency(agency).size();

		final int expectedNumberOfAgencyEmployeesAfter = numberOfAgencyEmployeesBefore + 1;

		// when
		agencyService.addEmployeeToAgency(agency, employee);

		// then
		assertTrue(expectedNumberOfAgencyEmployeesAfter == agencyService
				.findEmployeesOfAgency(agencyService.findAgencyByID(agencyID)).size());
		assertEquals(employee, agencyService.findEmployeesOfAgency(agencyService.findAgencyByID(agencyID))
				.get(expectedNumberOfAgencyEmployeesAfter - 1));
	}

	@Test
	public void shouldRemoveEmployeeFromAgency() {
		// given
		final int employeeID = 15;

		EmployeeEntity employee = employeeService.findEmployeeByID(employeeID);

		List<EmployeeEntity> employeesBefore = agencyService.findEmployeesOfAgency(employee.getAgency());

		final int numberOfAgencyEmployeesBefore = employeesBefore.size();
		final int expectedNumberOfAgencyEmployeesAfter = numberOfAgencyEmployeesBefore - 1;

		// when
		EmployeeEntity removedEmployee = agencyService.removeEmployeeFromAgency(employee);

		// then
		assertTrue(expectedNumberOfAgencyEmployeesAfter == agencyService.findEmployeesOfAgency(employee.getAgency())
				.size());
		assertEquals(employee, removedEmployee);
	}

	@Test
	public void shouldFindEmployeesOfAgency() {
		// given
		final int agencyID = 1;

		AgencyEntity agency = agencyService.findAgencyByID(agencyID);

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(4));
		expectedEmployees.add(employeeService.findEmployeeByID(5));
		expectedEmployees.add(employeeService.findEmployeeByID(11));
		expectedEmployees.add(employeeService.findEmployeeByID(20));
		expectedEmployees.add(employeeService.findEmployeeByID(23));
		expectedEmployees.add(employeeService.findEmployeeByID(24));

		// when
		List<EmployeeEntity> actualEmployees = agencyService.findEmployeesOfAgency(agency);

		// then
		assertEquals(expectedEmployees, actualEmployees);
	}

	@Test
	public void shouldFindSupervisorsOfCarInAgency() {
		// given
		final int carID = 2;
		final int agencyID = 1;

		CarEntity car = carService.findCarByID(carID);
		AgencyEntity agency = agencyService.findAgencyByID(agencyID);

		List<EmployeeEntity> expectedEmployees = new ArrayList<EmployeeEntity>();
		expectedEmployees.add(employeeService.findEmployeeByID(11));

		// when
		List<EmployeeEntity> actualEmployees = agencyService.findSupervisorsOfCarInAgency(agency, car);

		// then
		assertEquals(expectedEmployees, actualEmployees);
	}

}
