package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.CarEntity;
import com.capgemini.domain.CarSupervisorEntity;
import com.capgemini.domain.EmployeeEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CarServiceTest {

	@Autowired
	private CarService carService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private CarStatusService carStatusService;

	@Autowired
	private CarSupervisorService carSupervisorService;

	@Autowired
	private RentalDataService rentalDataService;

	@Test
	public void shouldFindAllCars() {
		// given

		// when
		List<CarEntity> allCars = carService.findAllCars();

		// then
		assertNotNull(allCars);
		assertTrue(130 == allCars.size());
	}

	@Test
	public void shouldFindCarById() {
		// given
		final int carID = 1;

		// when
		CarEntity car = carService.findCarByID(carID);

		// then
		assertNotNull(car);
		assertEquals("minivan", car.getCarBodyType());
		assertEquals("Mazda", car.getBrand());
		assertEquals("MPV", car.getModel());
		assertEquals(1994, car.getProductionYear());
		assertEquals("Aquamarine", car.getColour());
		assertEquals(new BigDecimal("1.3"), car.getEngineVolume());
		assertEquals(117, car.getPower());
		assertEquals(39521, car.getMileage());
		assertEquals("CV48673", car.getPlateNumber());
	}

	@Test
	public void shouldSaveCar() {
		// given
		CarEntity car = new CarEntity();
		car.setStatus(carStatusService.findCarStatusByID(1));
		car.setCarBodyType("testBodyType");
		car.setBrand("testBrand");
		car.setModel("testModel");
		car.setProductionYear(1991);
		car.setColour("testColour");
		car.setEngineVolume(new BigDecimal("1.0"));
		car.setPower(100);
		car.setMileage(100000);
		car.setPlateNumber("1234567");

		int sizeBefore = carService.findAllCars().size();

		// when
		carService.saveCar(car);
		List<CarEntity> cars = carService.findAllCars();

		// then
		assertEquals(car, cars.get(cars.size() - 1));
		assertTrue(sizeBefore + 1 == cars.size());
	}

	@Test
	public void shouldDeleteCar() {
		// given
		final int numberOfEmployeesBefore = employeeService.findAllEmployees().size();
		final int carID = 1;
		CarEntity car = carService.findCarByID(carID);

		int sizeBefore = carService.findAllCars().size();

		// when
		carService.deleteCar(car);
		List<CarEntity> cars = carService.findAllCars();

		// then
		assertNull(carService.findCarByID(carID));
		assertTrue(sizeBefore - 1 == cars.size());
		assertNull(carSupervisorService.findAllCarSupervisors().stream().filter(cs -> cs.getCar().equals(car)).findAny()
				.orElse(null));
		assertNull(rentalDataService.findAllRentalData().stream().filter(rd -> rd.getCar().equals(car)).findAny()
				.orElse(null));
		assertTrue(numberOfEmployeesBefore == employeeService.findAllEmployees().size());
	}

	@Test
	public void shouldUpdateCar() {
		// given
		final int carID = 1;

		CarEntity car = carService.findCarByID(carID);

		final String newColour = "Blue";
		final String oldColour = car.getColour();
		car.setColour(newColour);

		int sizeBefore = carService.findAllCars().size();

		// when
		carService.updateCar(car);
		List<CarEntity> cars = carService.findAllCars();

		// then
		assertTrue(newColour == carService.findCarByID(carID).getColour());
		assertTrue(newColour != oldColour);
		assertTrue(sizeBefore == cars.size());
	}

	@Test
	public void shouldFindCarsBySupervisor() {
		// given
		final int supervisorID = 5;

		EmployeeEntity supervisor = employeeService.findEmployeeByID(supervisorID);

		List<CarEntity> expectedCars = new ArrayList<CarEntity>();
		expectedCars.add(carService.findCarByID(49));
		expectedCars.add(carService.findCarByID(128));
		expectedCars.add(carService.findCarByID(47));
		expectedCars.add(carService.findCarByID(91));
		expectedCars.add(carService.findCarByID(20));
		expectedCars.add(carService.findCarByID(30));
		expectedCars.add(carService.findCarByID(74));
		expectedCars.add(carService.findCarByID(89));
		expectedCars.add(carService.findCarByID(94));
		expectedCars.add(carService.findCarByID(32));

		// when
		List<CarEntity> foundCars = carService.findCarsBySupervisor(supervisor);

		// then
		assertNotNull(foundCars.get(0));
		assertTrue(foundCars.size() == 10);
		assertEquals(expectedCars, foundCars);
	}

	@Test
	public void shouldFindCarsByBodyTypeAndBrand() {
		// given
		final String carBodyType = "SUV";
		final String carBrand = "Acura";

		List<CarEntity> expectedCars = new ArrayList<CarEntity>();
		expectedCars.add(carService.findCarByID(13));

		// when
		List<CarEntity> foundCars = carService.findCarsByBodyTypeAndBrand(carBodyType, carBrand);

		// then
		assertNotNull(foundCars);
		assertTrue(foundCars.size() == 1);
		assertEquals(expectedCars, foundCars);
	}

	@Test
	public void shouldFindCarsByBodyTypeOnly() {
		// given
		final String carBodyType = "SUV";
		final String carBrand = "";

		List<CarEntity> expectedCars = new ArrayList<CarEntity>();
		expectedCars.add(carService.findCarByID(3));
		expectedCars.add(carService.findCarByID(13));
		expectedCars.add(carService.findCarByID(28));
		expectedCars.add(carService.findCarByID(47));
		expectedCars.add(carService.findCarByID(49));
		expectedCars.add(carService.findCarByID(71));
		expectedCars.add(carService.findCarByID(82));
		expectedCars.add(carService.findCarByID(83));
		expectedCars.add(carService.findCarByID(96));
		expectedCars.add(carService.findCarByID(123));

		// when
		List<CarEntity> foundCars = carService.findCarsByBodyTypeAndBrand(carBodyType, carBrand);

		// then
		assertNotNull(foundCars);
		assertTrue(foundCars.size() == 10);
		assertEquals(expectedCars, foundCars);
	}

	@Test
	public void shouldFindCarsByBrandOnly() {
		// given
		final String carBodyType = "";
		final String carBrand = "Acura";

		List<CarEntity> expectedCars = new ArrayList<CarEntity>();
		expectedCars.add(carService.findCarByID(13));
		expectedCars.add(carService.findCarByID(105));

		// when
		List<CarEntity> foundCars = carService.findCarsByBodyTypeAndBrand(carBodyType, carBrand);

		// then
		assertNotNull(foundCars);
		assertTrue(foundCars.size() == 2);
		assertEquals(expectedCars, foundCars);
	}

	@Test
	public void shouldAssignCarSupervisor() {
		// given
		final int carID = 1;

		CarEntity car = carService.findCarByID(carID);

		final int employeeID = 1;

		EmployeeEntity employee = employeeService.findEmployeeByID(employeeID);

		Set<CarSupervisorEntity> supervisorsBefore = car.getSupervisors();

		int numberOfSupervisorsBefore = supervisorsBefore.size();
		int sizeBefore = carService.findAllCars().size();

		// when
		carService.assignSupervisor(car, employee);

		List<CarEntity> cars = carService.findAllCars();
		Set<CarSupervisorEntity> supervisors = car.getSupervisors();

		CarSupervisorEntity addedSupervisor = supervisors.stream()
				.max((s1, s2) -> s1.getDateSince().compareTo(s2.getDateSince())).orElse(null);

		// then
		assertEquals(car, addedSupervisor.getCar());
		assertEquals(employee, addedSupervisor.getEmployee());
		assertTrue(numberOfSupervisorsBefore + 1 == supervisors.size());
		assertTrue(sizeBefore == cars.size());
	}

	@Test
	public void shouldFindCarsRentedByMoreThan2DifferentCustomers() {
		// given
		final long minimalNumberOfDistinctRentals = 2;

		List<CarEntity> expectedCars = new ArrayList<CarEntity>();
		expectedCars.add(carService.findCarByID(3));
		expectedCars.add(carService.findCarByID(6));

		// when
		List<CarEntity> actualCars = carService.findCarsRentedByMoreThanXCustomers(minimalNumberOfDistinctRentals);

		// then
		assertNotNull(actualCars);
		assertEquals(expectedCars, actualCars);
	}

	@Test
	public void shouldCountCarsRentedBetweenDates() throws ParseException {
		// given
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		final Date dateSince = formatter.parse("2017-01-01");
		final Date dateTo = formatter.parse("2017-01-31");
		final long expectedNumber = 6;

		// when
		long actualNumber = carService.countCarsRentedBetweenDates(dateSince, dateTo);

		// then
		assertTrue(expectedNumber == actualNumber);
	}

}
