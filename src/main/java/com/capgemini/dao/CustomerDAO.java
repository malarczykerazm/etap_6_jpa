package com.capgemini.dao;

import com.capgemini.domain.CustomerEntity;

public interface CustomerDAO extends DAO<CustomerEntity, Integer> {

}
