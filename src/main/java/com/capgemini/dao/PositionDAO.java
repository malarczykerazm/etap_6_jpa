package com.capgemini.dao;

import com.capgemini.domain.PositionEntity;

public interface PositionDAO extends DAO<PositionEntity, Integer> {

}
