package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.CarStatusDAO;
import com.capgemini.domain.CarStatusEntity;

@Repository
public class CarStatusDAOImpl extends AbstractDAO<CarStatusEntity, Integer> implements CarStatusDAO {

}
