package com.capgemini.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.capgemini.dao.EmployeeDAO;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.rowmapper.EmployeeRowMapper;
import com.capgemini.searchcriteria.EmployeeSearchCriteria;

@Repository
public class EmployeeDAOImpl extends AbstractDAO<EmployeeEntity, Integer> implements EmployeeDAO {

	@Autowired
	private NamedParameterJdbcOperations jdbcTemplate;

	@Autowired
	private EmployeeRowMapper employeeRowMapper;

	private static final String _SELECT = "select e.id, e.firstname, e.surname, e.position_id, e.agency_id from employees e";
	private String FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL;

	@Override
	public List<EmployeeEntity> findEmployeeByAgencySupervisedCarAndPosition(EmployeeSearchCriteria eSC) {

		int agencyID = eSC.getAgencyID();
		int supervisedCarID = eSC.getSupervisedCarID();
		int positionID = eSC.getPositionID();

		FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = _SELECT;

		if (0 != supervisedCarID) {
			FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL
					+ " join car_supervisors cs on e.id = cs.employee_id";
		}

		if (0 != agencyID || 0 != supervisedCarID || 0 != positionID) {
			FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL + " where";
		}

		if (0 != agencyID) {
			FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL
					+ " e.agency_id = :agency_id";
		}

		if (0 != supervisedCarID) {
			if (0 != agencyID) {
				FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL + " and";
			}
			FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL
					+ " cs.car_id = :supervised_car_id";
		}

		if (0 != positionID) {
			if (0 != agencyID || 0 != supervisedCarID) {
				FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL + " and";
			}
			FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL
					+ " e.position_id = :position_id";
		}

		FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL = FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL + ";";

		SqlParameterSource params = new MapSqlParameterSource("agency_id", agencyID)
				.addValue("supervised_car_id", supervisedCarID).addValue("position_id", positionID);

		return jdbcTemplate.query(FIND_EMPLOYEES_BY_DEFINED_PARAMETERS_SQL, params, employeeRowMapper);

	}
}
