package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.ContactDataDAO;
import com.capgemini.domain.ContactDataEntity;

@Repository
public class ContactDataDAOImpl extends AbstractDAO<ContactDataEntity, Integer> implements ContactDataDAO {

}
