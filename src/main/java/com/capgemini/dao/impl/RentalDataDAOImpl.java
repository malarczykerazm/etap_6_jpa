package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.RentalDataDAO;
import com.capgemini.domain.RentalDataEntity;

@Repository
public class RentalDataDAOImpl extends AbstractDAO<RentalDataEntity, Integer> implements RentalDataDAO {

}
