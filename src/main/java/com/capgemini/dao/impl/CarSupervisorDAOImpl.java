package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.CarSupervisorDAO;
import com.capgemini.domain.CarSupervisorEntity;

@Repository
public class CarSupervisorDAOImpl extends AbstractDAO<CarSupervisorEntity, Integer> implements CarSupervisorDAO {

}
