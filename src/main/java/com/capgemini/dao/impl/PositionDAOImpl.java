package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.PositionDAO;
import com.capgemini.domain.PositionEntity;

@Repository
public class PositionDAOImpl extends AbstractDAO<PositionEntity, Integer> implements PositionDAO {

}
