package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.CustomerDAO;
import com.capgemini.domain.CustomerEntity;

@Repository
public class CustomerDAOImpl extends AbstractDAO<CustomerEntity, Integer> implements CustomerDAO {

}
