package com.capgemini.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.CarDAO;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;

@Repository
public class CarDAOImpl extends AbstractDAO<CarEntity, Integer> implements CarDAO {

	@Override
	public List<CarEntity> findCarsByBodyTypeAndBrand(String bodyType, String brand) {
		TypedQuery<CarEntity> query = entityManager.createNamedQuery("cars.findCarsByBodyTypeAndBrand",
				CarEntity.class);
		query.setParameter("body_type", bodyType);
		query.setParameter("brand", brand);
		return query.getResultList();
	}

	@Override
	public List<CarEntity> findCarsBySupervisor(EmployeeEntity employee) {
		TypedQuery<CarEntity> query = entityManager.createNamedQuery("car_supervisors.findCarsBySupervisor",
				CarEntity.class);
		query.setParameter("employee", employee);
		return query.getResultList();
	}

	@Override
	public List<CarEntity> findCarsRentedByMoreThanXCustomers(long x) {
		TypedQuery<CarEntity> query = entityManager.createNamedQuery("cars.findCarsRentedByMoreThanXCustomers",
				CarEntity.class);
		query.setParameter("x", x);
		return query.getResultList();
	}

	@Override
	public Long countCarsRentedBetweenDates(Date dateSince, Date dateTo) {
		TypedQuery<Long> query = entityManager.createNamedQuery("cars.findCarsBetweenDates", Long.class);
		query.setParameter("date_since", dateSince);
		query.setParameter("date_to", dateTo);
		return query.getSingleResult();
	}

}
