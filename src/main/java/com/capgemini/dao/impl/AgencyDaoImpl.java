package com.capgemini.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.AgencyDAO;
import com.capgemini.domain.AgencyEntity;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;

@Repository
public class AgencyDAOImpl extends AbstractDAO<AgencyEntity, Integer> implements AgencyDAO {

	@Override
	public List<EmployeeEntity> findSupervisorsOfCarInAgency(AgencyEntity agency, CarEntity car) {
		TypedQuery<EmployeeEntity> query = entityManager
				.createNamedQuery("car_supervisors.findSupervisorByCarAndAgency", EmployeeEntity.class);
		query.setParameter("car", car);
		query.setParameter("agency", agency);
		return query.getResultList();
	}

}
