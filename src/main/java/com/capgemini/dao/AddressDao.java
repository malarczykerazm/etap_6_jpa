package com.capgemini.dao;

import java.util.List;

import com.capgemini.domain.AddressEntity;

public interface AddressDAO extends DAO<AddressEntity, Integer> {

	List<AddressEntity> findAddressesByNumber(String addressNumber);

	List<AddressEntity> findAddressesByStreet(String street);

	List<AddressEntity> findAddressesByCity(String city);

	List<AddressEntity> findAddressesByPostcode(String postcode);

	List<AddressEntity> findAddressesByCountry(String country);
}
