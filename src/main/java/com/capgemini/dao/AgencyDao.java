package com.capgemini.dao;

import java.util.List;

import com.capgemini.domain.AgencyEntity;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;

public interface AgencyDAO extends DAO<AgencyEntity, Integer> {

	List<EmployeeEntity> findSupervisorsOfCarInAgency(AgencyEntity agency, CarEntity car);

}
