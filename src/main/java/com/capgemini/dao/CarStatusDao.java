package com.capgemini.dao;

import com.capgemini.domain.CarStatusEntity;

public interface CarStatusDAO extends DAO<CarStatusEntity, Integer> {

}
