package com.capgemini.dao;

import com.capgemini.domain.CarSupervisorEntity;

public interface CarSupervisorDAO extends DAO<CarSupervisorEntity, Integer> {

}
