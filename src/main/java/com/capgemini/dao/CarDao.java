package com.capgemini.dao;

import java.util.Date;
import java.util.List;

import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;

public interface CarDAO extends DAO<CarEntity, Integer> {

	List<CarEntity> findCarsByBodyTypeAndBrand(String bodyType, String brand);

	List<CarEntity> findCarsBySupervisor(EmployeeEntity employee);

	List<CarEntity> findCarsRentedByMoreThanXCustomers(long x);

	Long countCarsRentedBetweenDates(Date dateSince, Date dateTo);

}
