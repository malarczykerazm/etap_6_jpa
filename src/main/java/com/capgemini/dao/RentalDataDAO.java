package com.capgemini.dao;

import com.capgemini.domain.RentalDataEntity;

public interface RentalDataDAO extends DAO<RentalDataEntity, Integer> {

}
