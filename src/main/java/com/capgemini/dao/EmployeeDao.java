package com.capgemini.dao;

import java.util.List;

import com.capgemini.domain.EmployeeEntity;
import com.capgemini.searchcriteria.EmployeeSearchCriteria;

public interface EmployeeDAO extends DAO<EmployeeEntity, Integer> {

	List<EmployeeEntity> findEmployeeByAgencySupervisedCarAndPosition(EmployeeSearchCriteria eSC);

}
