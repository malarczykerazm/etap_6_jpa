package com.capgemini.service;

import com.capgemini.domain.ContactDataEntity;

public interface ContactDataService {

	ContactDataEntity findContactDataByID(int iD);

}
