package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.CarSupervisorEntity;

public interface CarSupervisorService {

	List<CarSupervisorEntity> findAllCarSupervisors();

}
