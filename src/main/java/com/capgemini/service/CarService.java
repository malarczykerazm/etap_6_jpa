package com.capgemini.service;

import java.util.Date;
import java.util.List;

import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;

public interface CarService {

	List<CarEntity> findAllCars();

	CarEntity findCarByID(int iD);

	CarEntity saveCar(CarEntity car);

	CarEntity deleteCar(CarEntity car);

	CarEntity updateCar(CarEntity car);

	List<CarEntity> findCarsByBodyTypeAndBrand(String bodyType, String brand);

	List<CarEntity> findCarsBySupervisor(EmployeeEntity employee);

	CarEntity assignSupervisor(CarEntity car, EmployeeEntity employee);

	List<CarEntity> findCarsRentedByMoreThanXCustomers(long x);

	Long countCarsRentedBetweenDates(Date dateSince, Date dateTo);

}
