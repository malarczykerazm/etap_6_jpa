package com.capgemini.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.ContactDataDAO;
import com.capgemini.domain.ContactDataEntity;
import com.capgemini.service.ContactDataService;

@Service
@Transactional(readOnly = false)
public class ContactDataServiceImpl implements ContactDataService {

	@Autowired
	private ContactDataDAO contactDataRepository;

	@Override
	public ContactDataEntity findContactDataByID(int iD) {
		return contactDataRepository.findOne(iD);
	}

}
