package com.capgemini.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.CarStatusDAO;
import com.capgemini.domain.CarStatusEntity;
import com.capgemini.service.CarStatusService;

@Service
@Transactional(readOnly = false)
public class CarStatusServiceImpl implements CarStatusService {

	@Autowired
	private CarStatusDAO carStatusRepository;

	@Override
	public CarStatusEntity findCarStatusByID(int iD) {
		return carStatusRepository.findOne(iD);
	}

	@Override
	public CarStatusEntity saveCarStatus(CarStatusEntity carStatus) {
		carStatus = carStatusRepository.save(carStatus);
		return carStatus;
	}

}
