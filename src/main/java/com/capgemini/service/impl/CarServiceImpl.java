package com.capgemini.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.CarDAO;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.CarSupervisorEntity;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.service.CarService;

@Service
@Transactional(readOnly = false)
public class CarServiceImpl implements CarService {

	@Autowired
	private CarDAO carRepository;

	@Override
	public List<CarEntity> findAllCars() {
		return carRepository.findAll();
	}

	@Override
	public CarEntity findCarByID(int iD) {
		return carRepository.findOne(iD);
	}

	@Override
	public CarEntity saveCar(CarEntity car) {
		car = carRepository.save(car);
		return car;
	}

	@Override
	public CarEntity deleteCar(CarEntity car) {
		carRepository.delete(car);
		return car;
	}

	@Override
	public CarEntity updateCar(CarEntity car) {
		carRepository.update(car);
		return car;
	}

	@Override
	public List<CarEntity> findCarsByBodyTypeAndBrand(String bodyType, String brand) {
		return carRepository.findCarsByBodyTypeAndBrand(bodyType, brand);
	}

	@Override
	public List<CarEntity> findCarsBySupervisor(EmployeeEntity employee) {
		return carRepository.findCarsBySupervisor(employee);
	}

	@Override
	public CarEntity assignSupervisor(CarEntity car, EmployeeEntity employee) {
		CarSupervisorEntity carSupervisor = new CarSupervisorEntity(employee, car);
		car.getSupervisors().add(carSupervisor);
		return carRepository.update(car);
	}

	@Override
	public List<CarEntity> findCarsRentedByMoreThanXCustomers(long x) {
		return carRepository.findCarsRentedByMoreThanXCustomers(x);
	}

	@Override
	public Long countCarsRentedBetweenDates(Date dateSince, Date dateTo) {
		return carRepository.countCarsRentedBetweenDates(dateSince, dateTo);
	}

}
