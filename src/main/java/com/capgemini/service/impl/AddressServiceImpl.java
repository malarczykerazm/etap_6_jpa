package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.AddressDAO;
import com.capgemini.domain.AddressEntity;
import com.capgemini.service.AddressService;

@Service
@Transactional(readOnly = false)
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressDAO addressRepository;

	@Override
	public List<AddressEntity> findAllAddresses() {
		return addressRepository.findAll();
	}

	@Override
	public List<AddressEntity> findAddressesByNumber(String addressNumber) {
		return addressRepository.findAddressesByNumber(addressNumber);
	}

	@Override
	public List<AddressEntity> findAddressesByStreet(String street) {
		return addressRepository.findAddressesByStreet(street);
	}

	@Override
	public List<AddressEntity> findAddressesByCity(String city) {
		return addressRepository.findAddressesByCity(city);
	}

	@Override
	public List<AddressEntity> findAddressesByPostcode(String postcode) {
		return addressRepository.findAddressesByPostcode(postcode);
	}

	@Override
	public List<AddressEntity> findAddressesByCountry(String country) {
		return addressRepository.findAddressesByCountry(country);
	}

	@Override
	public AddressEntity findAddressByID(int iD) {
		return addressRepository.findOne(iD);
	}

	@Override
	@Transactional(readOnly = false)
	public AddressEntity saveAddress(AddressEntity address) {
		address = addressRepository.save(address);
		return address;
	}

}
