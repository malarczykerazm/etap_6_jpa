package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.EmployeeDAO;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.searchcriteria.EmployeeSearchCriteria;
import com.capgemini.service.EmployeeService;

@Service
@Transactional(readOnly = false)
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDAO employeeRepository;

	@Override
	public List<EmployeeEntity> findAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public EmployeeEntity findEmployeeByID(int iD) {
		return employeeRepository.findOne(iD);
	}

	@Override
	public EmployeeEntity saveEmployee(EmployeeEntity employee) {
		employee = employeeRepository.save(employee);
		return employee;
	}

	@Override
	public List<EmployeeEntity> findEmployeesByAgencySupervisedCarAndPosition(EmployeeSearchCriteria eSC) {
		return employeeRepository.findEmployeeByAgencySupervisedCarAndPosition(eSC);
	}

}
