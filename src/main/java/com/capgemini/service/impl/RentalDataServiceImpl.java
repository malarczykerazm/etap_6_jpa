package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.RentalDataDAO;
import com.capgemini.domain.RentalDataEntity;
import com.capgemini.service.RentalDataService;

@Service
@Transactional(readOnly = false)
public class RentalDataServiceImpl implements RentalDataService {

	@Autowired
	private RentalDataDAO rentalDataRepository;

	@Override
	public List<RentalDataEntity> findAllRentalData() {
		return rentalDataRepository.findAll();
	}

}
