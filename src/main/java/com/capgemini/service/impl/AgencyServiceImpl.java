package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.AgencyDAO;
import com.capgemini.domain.AgencyEntity;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.service.AgencyService;

@Service
@Transactional(readOnly = false)
public class AgencyServiceImpl implements AgencyService {

	@Autowired
	private AgencyDAO agencyRepository;

	@Override
	public List<AgencyEntity> findAllAgencies() {
		return agencyRepository.findAll();
	}

	@Override
	public AgencyEntity findAgencyByID(int iD) {
		return agencyRepository.findOne(iD);
	}

	@Override
	public AgencyEntity saveAgency(AgencyEntity agency) {
		agency = agencyRepository.save(agency);
		return agency;
	}

	@Override
	public AgencyEntity deleteAgency(AgencyEntity agency) {
		agencyRepository.delete(agency);
		return agency;
	}

	@Override
	public AgencyEntity updateAgency(AgencyEntity agency) {
		agencyRepository.update(agency);
		return agency;
	}

	@Override
	public AgencyEntity addEmployeeToAgency(AgencyEntity agency, EmployeeEntity employee) {
		agency.getEmployees().add(employee);
		return agencyRepository.update(agency);
	}

	@Override
	public EmployeeEntity removeEmployeeFromAgency(EmployeeEntity employee) {
		AgencyEntity agency = employee.getAgency();
		agency.getEmployees().remove(employee);
		agencyRepository.update(agency);
		return employee;
	}

	@Override
	public List<EmployeeEntity> findEmployeesOfAgency(AgencyEntity agency) {
		return agency.getEmployees();
	}

	@Override
	public List<EmployeeEntity> findSupervisorsOfCarInAgency(AgencyEntity agency, CarEntity car) {
		return agencyRepository.findSupervisorsOfCarInAgency(agency, car);
	}

}
