package com.capgemini.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.PositionDAO;
import com.capgemini.domain.PositionEntity;
import com.capgemini.service.PositionService;

@Service
@Transactional(readOnly = false)
public class PositionServiceImpl implements PositionService {

	@Autowired
	private PositionDAO positionRepository;

	@Override
	public PositionEntity findPositionByID(int iD) {
		return positionRepository.findOne(iD);
	}

}
