package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.CustomerDAO;
import com.capgemini.domain.CustomerEntity;
import com.capgemini.service.CustomerService;

@Service
@Transactional(readOnly = false)
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerRepository;

	@Override
	public List<CustomerEntity> findAllCustomers() {
		return customerRepository.findAll();
	}

}
