package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.CarSupervisorDAO;
import com.capgemini.domain.CarSupervisorEntity;
import com.capgemini.service.CarSupervisorService;

@Service
@Transactional(readOnly = false)
public class CarSupervisorServiceImpl implements CarSupervisorService {

	@Autowired
	private CarSupervisorDAO carSupervisorRepository;

	@Override
	public List<CarSupervisorEntity> findAllCarSupervisors() {
		return carSupervisorRepository.findAll();
	}

}
