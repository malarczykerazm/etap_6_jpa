package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.CustomerEntity;

public interface CustomerService {

	List<CustomerEntity> findAllCustomers();

}
