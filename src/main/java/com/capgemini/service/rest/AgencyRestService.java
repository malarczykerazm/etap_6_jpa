package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.domain.AgencyEntity;
import com.capgemini.service.AgencyService;

@RestController
@RequestMapping(value = "/agencies")
public class AgencyRestService {

	@Autowired
	private AgencyService agencyService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<AgencyEntity> findAllAgencies() {
		return agencyService.findAllAgencies();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public AgencyEntity findAgencyByID(@PathVariable("id") int iD) {
		return agencyService.findAgencyByID(iD);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public AgencyEntity saveAgency(@RequestBody AgencyEntity agency) {
		return agencyService.saveAgency(agency);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public AgencyEntity deleteAgency(@PathVariable("id") int iD) {
		return agencyService.deleteAgency(agencyService.findAgencyByID(iD));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public AgencyEntity updateCar(@RequestBody AgencyEntity agency) {
		return agencyService.updateAgency(agency);
	}

}
