package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.domain.CarEntity;
import com.capgemini.service.CarService;

@RestController
@RequestMapping(value = "/cars")
public class CarRestService {

	@Autowired
	private CarService carService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<CarEntity> findAllCars() {
		return carService.findAllCars();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public CarEntity findCarByID(@PathVariable("id") int iD) {
		return carService.findCarByID(iD);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public CarEntity saveCar(@RequestBody CarEntity car) {
		return carService.saveCar(car);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public CarEntity deleteCar(@PathVariable("id") int iD) {
		return carService.deleteCar(carService.findCarByID(iD));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public CarEntity updateCar(@RequestBody CarEntity car) {
		return carService.updateCar(car);
	}

}
