package com.capgemini.service;

import com.capgemini.domain.CarStatusEntity;

public interface CarStatusService {

	CarStatusEntity findCarStatusByID(int iD);

	CarStatusEntity saveCarStatus(CarStatusEntity carStatus);
}
