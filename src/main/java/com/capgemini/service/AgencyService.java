package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.AgencyEntity;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;

public interface AgencyService {

	List<AgencyEntity> findAllAgencies();

	AgencyEntity findAgencyByID(int iD);

	AgencyEntity saveAgency(AgencyEntity agency);

	AgencyEntity deleteAgency(AgencyEntity agency);

	AgencyEntity updateAgency(AgencyEntity agency);

	AgencyEntity addEmployeeToAgency(AgencyEntity agency, EmployeeEntity employee);

	EmployeeEntity removeEmployeeFromAgency(EmployeeEntity employee);

	List<EmployeeEntity> findEmployeesOfAgency(AgencyEntity agency);

	List<EmployeeEntity> findSupervisorsOfCarInAgency(AgencyEntity agency, CarEntity car);

}
