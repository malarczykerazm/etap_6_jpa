package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.AddressEntity;

public interface AddressService {

	List<AddressEntity> findAllAddresses();

	List<AddressEntity> findAddressesByNumber(String addressNumber);

	List<AddressEntity> findAddressesByStreet(String street);

	List<AddressEntity> findAddressesByCity(String city);

	List<AddressEntity> findAddressesByPostcode(String postcode);

	List<AddressEntity> findAddressesByCountry(String country);

	AddressEntity findAddressByID(int iD);

	AddressEntity saveAddress(AddressEntity address);
}
