package com.capgemini.service;

import com.capgemini.domain.PositionEntity;

public interface PositionService {

	PositionEntity findPositionByID(int iD);

}
