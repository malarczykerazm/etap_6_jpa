package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.RentalDataEntity;

public interface RentalDataService {

	List<RentalDataEntity> findAllRentalData();

}
