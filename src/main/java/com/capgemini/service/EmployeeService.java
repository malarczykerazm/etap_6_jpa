package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.EmployeeEntity;
import com.capgemini.searchcriteria.EmployeeSearchCriteria;

public interface EmployeeService {

	List<EmployeeEntity> findAllEmployees();

	EmployeeEntity findEmployeeByID(int iD);

	EmployeeEntity saveEmployee(EmployeeEntity employee);

	List<EmployeeEntity> findEmployeesByAgencySupervisedCarAndPosition(EmployeeSearchCriteria eSC);
}
