package com.capgemini.domain;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "rental_data")
public class RentalDataEntity extends AbstractEntity {

	private static final long serialVersionUID = 1331856496749013534L;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private CustomerEntity customer;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "car_id")
	private CarEntity car;

	@NotNull
	@Column(name = "date_since")
	private Date dateSince;

	@NotNull
	@Column(name = "date_to")
	private Date dateTo;

	@ManyToOne
	@JoinColumn(name = "from_agency_id")
	private AgencyEntity fromAgency;

	@ManyToOne
	@JoinColumn(name = "to_agency_id")
	private AgencyEntity toAgency;

	@NotNull
	@Column(name = "price", columnDefinition = "DECIMAL(5,2)")
	private BigDecimal price = new BigDecimal("100.00");

	public CustomerEntity getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	public CarEntity getCar() {
		return car;
	}

	public void setCar(CarEntity car) {
		this.car = car;
	}

	public Date getDateSince() {
		return dateSince;
	}

	public void setDateSince(Date dateSince) {
		this.dateSince = dateSince;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public AgencyEntity getFromAgency() {
		return fromAgency;
	}

	public void setFromAgency(AgencyEntity fromAgency) {
		this.fromAgency = fromAgency;
	}

	public AgencyEntity getToAgency() {
		return toAgency;
	}

	public void setToAgency(AgencyEntity toAgency) {
		this.toAgency = toAgency;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
