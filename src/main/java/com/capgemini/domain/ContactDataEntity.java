package com.capgemini.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "contact_data")
public class ContactDataEntity extends AbstractEntity {

	private static final long serialVersionUID = 5898126542893426434L;

	@NotNull
	@Size(max = 20)
	@Column(name = "phone")
	private String phone;

	@NotNull
	@Size(max = 45)
	@Column(name = "email", unique = true)
	private String email;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
