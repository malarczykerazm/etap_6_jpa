package com.capgemini.domain;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "customers")
public class CustomerEntity extends AbstractEntity {

	private static final long serialVersionUID = -3727609120535252822L;

	@NotNull
	@Size(max = 80)
	@Column(name = "firstname")
	private String firstname;

	@Size(max = 80)
	@Column(name = "surname")
	private String surname;

	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@ManyToOne
	@JoinColumn(name = "agency_id")
	private AgencyEntity agency;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "correspondence_address_id")
	private AddressEntity correspondenceAddress;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "receipt_address_id")
	private AddressEntity receiptAddress;

	@NotNull
	@OneToOne
	@JoinColumn(name = "contact_id")
	private ContactDataEntity contactData;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public AgencyEntity getAgency() {
		return agency;
	}

	public void setAgency(AgencyEntity agency) {
		this.agency = agency;
	}

	public AddressEntity getCorrepsondenceAddress() {
		return correspondenceAddress;
	}

	public void setCorrepsondenceAddress(AddressEntity correpsondenceAddress) {
		this.correspondenceAddress = correpsondenceAddress;
	}

	public AddressEntity getReceiptAddress() {
		return receiptAddress;
	}

	public void setReceiptAddress(AddressEntity receiptAddress) {
		this.receiptAddress = receiptAddress;
	}

	public AddressEntity getCorrespondenceAddress() {
		return correspondenceAddress;
	}

	public void setCorrespondenceAddress(AddressEntity correspondenceAddress) {
		this.correspondenceAddress = correspondenceAddress;
	}

	public ContactDataEntity getContactData() {
		return contactData;
	}

	public void setContactData(ContactDataEntity contactData) {
		this.contactData = contactData;
	}

}
