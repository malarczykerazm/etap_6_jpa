package com.capgemini.domain;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "employees")
public class EmployeeEntity extends AbstractEntity {

	private static final long serialVersionUID = -6819437084409956036L;

	@NotNull
	@Size(max = 20)
	@Column(name = "firstname")
	private String firstname;

	@NotNull
	@Size(max = 80)
	@Column(name = "surname")
	private String surname;

	@NotNull
	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "agency_id")
	private AgencyEntity agency;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "address_id")
	private AddressEntity address;

	@NotNull
	@OneToOne
	@JoinColumn(name = "contact_id")
	private ContactDataEntity contactData;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "position_id")
	private PositionEntity position;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "employee")
	private List<CarSupervisorEntity> carSupervisor;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public AgencyEntity getAgency() {
		return agency;
	}

	public void setAgency(AgencyEntity agency) {
		this.agency = agency;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public ContactDataEntity getContactData() {
		return contactData;
	}

	public void setContactData(ContactDataEntity contactData) {
		this.contactData = contactData;
	}

	public PositionEntity getPosition() {
		return position;
	}

	public void setPosition(PositionEntity position) {
		this.position = position;
	}

	public List<CarSupervisorEntity> getCarSupervisor() {
		return carSupervisor;
	}

	public void setCarSupervisor(List<CarSupervisorEntity> carSupervisor) {
		this.carSupervisor = carSupervisor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((agency == null) ? 0 : agency.hashCode());
		result = prime * result + ((contactData == null) ? 0 : contactData.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeEntity other = (EmployeeEntity) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (agency == null) {
			if (other.agency != null)
				return false;
		} else if (!agency.equals(other.agency))
			return false;
		if (contactData == null) {
			if (other.contactData != null)
				return false;
		} else if (!contactData.equals(other.contactData))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

}
