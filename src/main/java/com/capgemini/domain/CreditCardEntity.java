package com.capgemini.domain;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "credit_cards")
public class CreditCardEntity extends AbstractEntity {

	private static final long serialVersionUID = 5801681497159856935L;

	@NotNull
	@OneToOne
	@JoinColumn(name = "customer_id")
	private CustomerEntity customer;

	@NotNull
	@Column(name = "card_number", unique = true)
	@Size(max = 16)
	private String cardNumber;

	@NotNull
	@Column(name = "expiration_date")
	private Date expirationDate;

	public CustomerEntity getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

}
