package com.capgemini.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "addresses")
@NamedQueries({
		@NamedQuery(name = "addresses.findByNumber", query = "select a from AddressEntity a where a.addressNumber = :address_number"),
		@NamedQuery(name = "addresses.findByStreet", query = "select a from AddressEntity a where upper(a.street) like concat('%', upper(:street), '%')"),
		@NamedQuery(name = "addresses.findByCity", query = "select a from AddressEntity a where upper(a.city) like concat('%', upper(:city), '%')"),
		@NamedQuery(name = "addresses.findByPostcode", query = "select a from AddressEntity a where a.postcode = :postcode"),
		@NamedQuery(name = "addresses.findByCountry", query = "select a from AddressEntity a where upper(a.country) like concat('%', upper(:country), '%')") })
public class AddressEntity extends AbstractEntity {

	private static final long serialVersionUID = -309692233828238728L;

	@NotNull
	@Size(max = 10)
	@Column(name = "address_number")
	private String addressNumber;

	@NotNull
	@Size(max = 45)
	@Column(name = "street")
	private String street;

	@NotNull
	@Size(max = 10)
	@Column(name = "postcode")
	private String postcode;

	@NotNull
	@Size(max = 45)
	@Column(name = "city")
	private String city;

	@NotNull
	@Size(max = 45)
	@Column(name = "country")
	private String country;

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
