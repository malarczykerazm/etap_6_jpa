package com.capgemini.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "car_statuses")
public class CarStatusEntity extends AbstractEntity {

	private static final long serialVersionUID = 5582189491910476693L;

	@NotNull
	@Size(max = 45)
	@Column(name = "car_status")
	private String carStatus;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "status")
	@JsonBackReference
	private Set<CarEntity> cars;

	public String getCarStatus() {
		return carStatus;
	}

	public void setCarStatus(String carStatus) {
		this.carStatus = carStatus;
	}

	public Set<CarEntity> getCars() {
		return cars;
	}

	public void setCars(Set<CarEntity> cars) {
		this.cars = cars;
	}

}
