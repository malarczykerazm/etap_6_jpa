package com.capgemini.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "agencies")
public class AgencyEntity extends AbstractEntity {

	private static final long serialVersionUID = 1981243022400188804L;

	@NotNull
	@OneToOne(orphanRemoval = true, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "address_id")
	@JsonManagedReference
	private AddressEntity address;

	@NotNull
	@OneToOne(orphanRemoval = true, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "contact_id")
	@JsonManagedReference
	private ContactDataEntity contactData;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "agency")
	@JsonIgnore
	private List<EmployeeEntity> employees;

	@OneToMany(mappedBy = "agency")
	@JsonIgnore
	private List<CustomerEntity> customers;

	@OneToMany(mappedBy = "fromAgency")
	@JsonIgnore
	private List<RentalDataEntity> rentalDataFromAgency;

	@OneToMany(mappedBy = "toAgency")
	@JsonIgnore
	private List<RentalDataEntity> rentalDataToAgency;

	@PreRemove
	private void setDataToNull() {
		setAgencyInCustomerToNull();
		setFromAgencyInRentalDataToNull();
		setToAgencyInRentalDataToNull();
	}

	private void setAgencyInCustomerToNull() {
		for (CustomerEntity customer : customers) {
			customer.setAgency(null);
		}
	}

	private void setFromAgencyInRentalDataToNull() {
		for (RentalDataEntity rentalData : rentalDataFromAgency) {
			rentalData.setFromAgency(null);
		}
	}

	private void setToAgencyInRentalDataToNull() {
		for (RentalDataEntity rentalData : rentalDataToAgency) {
			rentalData.setToAgency(null);
		}
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public ContactDataEntity getContactData() {
		return contactData;
	}

	public void setContactData(ContactDataEntity contactData) {
		this.contactData = contactData;
	}

	public List<EmployeeEntity> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeEntity> employees) {
		this.employees = employees;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((contactData == null) ? 0 : contactData.hashCode());
		result = prime * result + ((customers == null) ? 0 : customers.hashCode());
		result = prime * result + ((employees == null) ? 0 : employees.hashCode());
		result = prime * result + ((rentalDataFromAgency == null) ? 0 : rentalDataFromAgency.hashCode());
		result = prime * result + ((rentalDataToAgency == null) ? 0 : rentalDataToAgency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgencyEntity other = (AgencyEntity) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (contactData == null) {
			if (other.contactData != null)
				return false;
		} else if (!contactData.equals(other.contactData))
			return false;
		if (customers == null) {
			if (other.customers != null)
				return false;
		} else if (!customers.equals(other.customers))
			return false;
		if (employees == null) {
			if (other.employees != null)
				return false;
		} else if (!employees.equals(other.employees))
			return false;
		if (rentalDataFromAgency == null) {
			if (other.rentalDataFromAgency != null)
				return false;
		} else if (!rentalDataFromAgency.equals(other.rentalDataFromAgency))
			return false;
		if (rentalDataToAgency == null) {
			if (other.rentalDataToAgency != null)
				return false;
		} else if (!rentalDataToAgency.equals(other.rentalDataToAgency))
			return false;
		return true;
	}

}
