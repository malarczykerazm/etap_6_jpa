package com.capgemini.domain;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "car_supervisors")
@NamedQueries({
		@NamedQuery(name = "car_supervisors.findCarsBySupervisor", query = "select car from CarSupervisorEntity cs where cs.employee = :employee"),
		@NamedQuery(name = "car_supervisors.findSupervisorByCarAndAgency", query = "select employee from CarSupervisorEntity cs where cs.car = :car and cs.employee.agency = :agency") })
public class CarSupervisorEntity extends AbstractEntity {

	private static final long serialVersionUID = 4887872663496176908L;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "employee_id")
	private EmployeeEntity employee;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "car_id")
	private CarEntity car;

	@NotNull
	@Column(name = "date_since")
	private Date dateSince;

	@Column(name = "date_to")
	private Date dateTo;

	public CarSupervisorEntity() {

	}

	public CarSupervisorEntity(EmployeeEntity employee, CarEntity car) {
		super();
		this.employee = employee;
		this.car = car;
		this.dateSince = Date.valueOf(LocalDate.now());
	}

	public EmployeeEntity getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeEntity employee) {
		this.employee = employee;
	}

	public CarEntity getCar() {
		return car;
	}

	public void setCar(CarEntity car) {
		this.car = car;
	}

	public Date getDateSince() {
		return dateSince;
	}

	public void setDateSince(Date dateSince) {
		this.dateSince = dateSince;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((car == null) ? 0 : car.hashCode());
		result = prime * result + ((dateSince == null) ? 0 : dateSince.hashCode());
		result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
		result = prime * result + ((employee == null) ? 0 : employee.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarSupervisorEntity other = (CarSupervisorEntity) obj;
		if (car == null) {
			if (other.car != null)
				return false;
		} else if (!car.equals(other.car))
			return false;
		if (dateSince == null) {
			if (other.dateSince != null)
				return false;
		} else if (!dateSince.equals(other.dateSince))
			return false;
		if (dateTo == null) {
			if (other.dateTo != null)
				return false;
		} else if (!dateTo.equals(other.dateTo))
			return false;
		if (employee == null) {
			if (other.employee != null)
				return false;
		} else if (!employee.equals(other.employee))
			return false;
		return true;
	}

}
