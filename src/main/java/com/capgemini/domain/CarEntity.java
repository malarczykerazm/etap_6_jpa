package com.capgemini.domain;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "cars")
@NamedQueries({
		@NamedQuery(name = "cars.findCarsByBodyTypeAndBrand", query = "select c from CarEntity c where upper(c.carBodyType) like concat('%', upper(:body_type), '%') and upper(c.brand) like concat('%', upper(:brand), '%')"),
		@NamedQuery(name = "cars.findCarsRentedByMoreThanXCustomers", query = "select car from CarEntity car join car.rentalData rd group by car having count(distinct rd.customer) > :x"),
		@NamedQuery(name = "cars.findCarsBetweenDates", query = "select count(distinct c) from CarEntity c join c.rentalData rd where (rd.dateSince between :date_since and :date_to) and (rd.dateTo between :date_since and :date_to)") })
public class CarEntity extends AbstractEntity {

	private static final long serialVersionUID = 5378057831320272772L;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "status_id")
	@JsonManagedReference
	private CarStatusEntity status;

	@NotNull
	@Size(max = 25)
	@Column(name = "car_body_type")
	private String carBodyType;

	@NotNull
	@Size(max = 45)
	@Column(name = "brand")
	private String brand;

	@NotNull
	@Size(max = 45)
	@Column(name = "model")
	private String model;

	@NotNull
	@Column(name = "production_year")
	private int productionYear;

	@NotNull
	@Size(max = 45)
	@Column(name = "colour")
	private String colour;

	@NotNull
	@Column(name = "engine_volume", columnDefinition = "DECIMAL(2,1)")
	private BigDecimal engineVolume;

	@NotNull
	@Column(name = "power")
	private int power;

	@NotNull
	@Column(name = "mileage")
	private int mileage;

	@NotNull
	@Size(max = 7)
	@Column(name = "plate_number")
	private String plateNumber;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "car")
	@JsonIgnore
	private Set<CarSupervisorEntity> supervisors;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "car")
	@JsonIgnore
	private Set<RentalDataEntity> rentalData;

	public CarStatusEntity getStatus() {
		return status;
	}

	public void setStatus(CarStatusEntity status) {
		this.status = status;
	}

	public String getCarBodyType() {
		return carBodyType;
	}

	public void setCarBodyType(String carBodyType) {
		this.carBodyType = carBodyType;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(int productionYear) {
		this.productionYear = productionYear;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public BigDecimal getEngineVolume() {
		return engineVolume;
	}

	public void setEngineVolume(BigDecimal engineVolume) {
		this.engineVolume = engineVolume;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public Set<CarSupervisorEntity> getSupervisors() {
		return supervisors;
	}

	public void setSupervisors(Set<CarSupervisorEntity> supervisors) {
		this.supervisors = supervisors;
	}

	public Set<RentalDataEntity> getRentalData() {
		return rentalData;
	}

	public void setRentalData(Set<RentalDataEntity> rentalData) {
		this.rentalData = rentalData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + ((carBodyType == null) ? 0 : carBodyType.hashCode());
		result = prime * result + ((colour == null) ? 0 : colour.hashCode());
		result = prime * result + ((engineVolume == null) ? 0 : engineVolume.hashCode());
		result = prime * result + mileage;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((plateNumber == null) ? 0 : plateNumber.hashCode());
		result = prime * result + power;
		result = prime * result + productionYear;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarEntity other = (CarEntity) obj;
		if (brand == null) {
			if (other.brand != null)
				return false;
		} else if (!brand.equals(other.brand))
			return false;
		if (carBodyType == null) {
			if (other.carBodyType != null)
				return false;
		} else if (!carBodyType.equals(other.carBodyType))
			return false;
		if (colour == null) {
			if (other.colour != null)
				return false;
		} else if (!colour.equals(other.colour))
			return false;
		if (engineVolume == null) {
			if (other.engineVolume != null)
				return false;
		} else if (!engineVolume.equals(other.engineVolume))
			return false;
		if (mileage != other.mileage)
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (plateNumber == null) {
			if (other.plateNumber != null)
				return false;
		} else if (!plateNumber.equals(other.plateNumber))
			return false;
		if (power != other.power)
			return false;
		if (productionYear != other.productionYear)
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

}
