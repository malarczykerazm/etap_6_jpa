package com.capgemini.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.capgemini.domain.EmployeeEntity;
import com.capgemini.service.AgencyService;
import com.capgemini.service.PositionService;

@Component
public class EmployeeRowMapper implements RowMapper<EmployeeEntity> {

	@Autowired
	private PositionService positionService;

	@Autowired
	private AgencyService agencyService;

	@Override
	public EmployeeEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmployeeEntity employee = new EmployeeEntity();
		employee.setID(rs.getInt("ID"));
		employee.setFirstname(rs.getString("firstname"));
		employee.setSurname(rs.getString("surname"));
		employee.setPosition(positionService.findPositionByID(rs.getInt("position_id")));
		employee.setAgency(agencyService.findAgencyByID(rs.getInt("agency_id")));
		return employee;
	}

}
