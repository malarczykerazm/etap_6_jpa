package com.capgemini.searchcriteria;

public class EmployeeSearchCriteria {

	private int agencyID;
	private int supervisedCarID;
	private int positionID;

	public int getAgencyID() {
		return agencyID;
	}

	public void setAgencyID(int agencyID) {
		this.agencyID = agencyID;
	}

	public int getSupervisedCarID() {
		return supervisedCarID;
	}

	public void setSupervisedCarID(int supervisedCarID) {
		this.supervisedCarID = supervisedCarID;
	}

	public int getPositionID() {
		return positionID;
	}

	public void setPositionID(int positionID) {
		this.positionID = positionID;
	}

}
